#!/bin/bash
# 2. Mostrar els arguments rebuts línia a línia, tot numerànt-los

contador=0
lista_args=$*

for arg in $lista_args
do
  echo "$contador: $arg"
  ((contador++))
done
exit 0

