# .8 Fer un programa que rep com a argument noms d'usuari, si existeixen en el sistema
# (en el fitxer /etc/passwd) mostra el nom per stdout. Si no existeix el mostra per stderr.


for user in $*
do
	grep -q "^$user:" /etc/passwd
	if [ $? -eq 0 ]
	then
  	  echo "$user"
	else
	  echo "Error: no se ha encontrado el usuario $user" >> /dev/stderr
	fi
done
exit 0
