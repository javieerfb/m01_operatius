#!/bin/bash
# 1) Mostrar l'entrada estàndard numerant línia a línia

contador=0
# Ejecuta todo lo que esté dentro del while cada vez que encuentre 1 línea
while read -r line
do
  ((contador++))
  echo "$contador: $line" 2>/dev/null
done
exit 0
