#!/bin/bash

#sinopsis: prog dir
#Validar que es rep un argument, que sigui un directori i llistar-ne el contigut.
#Per llistar el contingut 

# 1) Validar que sea 1 argumento
if [ $# -ne 1 ] ; then
	echo "Error: num args no valido"
	echo "Usage: $0 directorio"
	exit 1
fi

# 2) Validar que sea directorio
if [ ! -d $1 ] ; then
	echo "Error: $1 no es un directorio"
	exit 1
fi

# 3) listar el contenido
ruta=$1
lista=$(ls $1)
for elemento in $lista
do
	if [ -d $1$elemento ] ; then
		echo "$elemento es un directorio"
	elif [ -f $1$elemento ] ; then
		echo "$elemento es un fichero"
	elif [ -h $1$elemento ] ; then
		echo "$elemento es un enlace simbolico"
	else
		echo "$elemento no es ni fichero regular, ni directorio, ni enlace"
	fi
	
done
exit 0
