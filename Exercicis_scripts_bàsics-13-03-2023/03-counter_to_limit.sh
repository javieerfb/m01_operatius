#!/bin/bash
# 3. Fer un comptador des de zero fins al valor indicat per l'argument rebut

MAX=$1
contador=0

while [ $contador -le $MAX ] #Ej. Mientras que limite 10 >= 9 ($contador en su 9ª iteración), vuelve a mostrar $contador 
do
	echo "$contador"
	((contador++))
done
exit 0
