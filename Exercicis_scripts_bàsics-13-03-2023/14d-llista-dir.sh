#!/bin/bash

#sinopsis: prog dir
#Validar que es rep un argument, que sigui un directori i llistar-ne el contigut.
#Per llistar el contingut 

# 1) Validar que sea 1 argumento
if [ $# -eq 0 ] ; then
  echo "Error: debe haber 1 o más argumentos"
  echo "Usage: $0 directorio"
  exit 1
fi

# 2) listar el contenido
ruta=$1
lista=$(ls $1)
lista_dirs=$*

for dir in $lista_dirs     #Comprobar que los argumentos sean directorios
do
  if [ ! -d $arg ] ; then
    echo "$arg no es un directorio" 1>&2	#Lo que envias a stdouw lo envias a stderr

  else
    for elemento in $lista
    do
      echo -e "$1:\n"
      
      if [ -d $1$elemento ] ; then
        echo -e "\t$elemento es un directorio"
      
      elif [ -f $1$elemento ] ; then
        echo -e "\t$elemento es un fichero"
      
      elif [ -h $1$elemento ] ; then
        echo -e "\t$elemento es un enlace simbolico"
      
      else
        echo -e "\t$elemento no es ni fichero regular, ni directorio, ni enlace"
      fi
  
    done
  fi
done
exit 0
