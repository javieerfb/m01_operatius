# 10. Fer un programa que rep com a argument un número indicatiu del número màxim de
# línies a mostrar. El programa processa stdin línia a línia i mostra numerades un
# màxim de num línies.

max=$1
numerador=1

while read -r line
do
	if [ $numerador -le $max ]
		then
			echo "$numerador: $line"
			((numerador++))
	else
		exit 0
	fi
done
exit 0
