#!/bin/bash
# 4. Fer un programa que rep com a arguments números de més (un o més) i indica per
# a cada mes rebut quants dies té el més.

lista_meses=$*

for mes in $lista_meses
do
	case $mes in
		2)
			echo "El mes $mes, tiene 28 o 29 dias"
		;;
		4|6|9|11)
			echo "El mes $mes, tiene 30 dias"
		;;
		*)
			echo "El mes $mes, tiene 31 dias"
		;;
	esac
done
exit 0
