# 7. Processar línia a línia l’entrada estàndard, si la línia té més de 60 caràcters la
# mostra, si no no.

while read -r line
do
	echo $line | grep -E '^.{60,}$'
done
exit 0
