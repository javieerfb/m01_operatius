# .9 Fer un programa que rep per stdin noms d'usuari (un per línia), si existeixen en el sistema
# (en el fitxer /etc/passwd) mostra el nom per stdout. Si no existeix el mostra per stderr.


while read -r line
do 
	grep -w "^$line" /etc/passwd &>/dev/null
	if [ $? -eq 0 ]
	then
	  grep -w "^$line" /etc/passwd
	else
	  echo "Error: no se ha encontrado el usuario $line"
	fi
done
exit 0
