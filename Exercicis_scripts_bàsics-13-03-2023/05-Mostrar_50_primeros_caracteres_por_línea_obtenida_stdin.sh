#!/bin/bash
# 5. Mostrar línia a línia l’entrada estàndard, retallant només els primers 50 caràcters.

while read -r line
do
	echo $line | cut -c1-50
done
exit 0
