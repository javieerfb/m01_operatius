#!/bin/bash
# 1HISX
# 13 Marzo 2023
# Descripción
# 6. Fer un programa que rep com a arguments noms de dies de la setmana i mostra
# quants dies eren laborables i quants festius. Si l’argument no és un dia de la
# setmana genera un error per stderr.
# Exemple: $ prog dilluns divendres dilluns dimarts kakota dissabte sunday

# 1) Validar que por lo menos hay 1 argumento
if [ $# -eq 0 ]
then
  echo "Error: numero args incorrecto"
  echo "Usage: $0 file"
  exit 1
fi

# 2) Chicha
contador_laborables=0
contador_festivos=0

for dia in $*
do
  case $dia in
    "dilluns"|"dimarts"|"dimecres"|"dijous"|"divendres")
	    ((contador_laborables++))
      ;;
    "dissabte"|"diumenge")
	    ((contador_festivos++))
      ;;
    *)
	  echo "Error: $dia no és un dia válid"
	  echo "Usage: dia debe ser: dilluns|dimarts|dimecres|dijous|divendres|dissabte|diumenge" 
  esac
done
echo "Laborables: $contador_laborables"
echo "Festivos: $contador_festivos"
exit 0
