#!/bin/bash
# @edt ASIX M01
# 16 Febrero 2023
# 
# Ejemplo de procesar argumentos
# ----------------------------------------------------
## TEORIA:
# $* y $@ --> son la lista de argumentos
# $1 y $2 --> son el primero y el 2o argumento respectivamente
# $$ --> el PID del programa
# $# --> número total de argumentos

echo '$*: ' $*
echo '$@: ' $@
echo '$#: ' $#
echo '$0: ' $0
echo '$1: ' $1
echo '$2: ' $2
echo '$9: ' $9 
echo '$10: ' ${10}
echo '$11: ' ${11}
echo '$$: ' $$
nom='puig'
echo "${nom}demworld"

# ENTRADA:
# ----------------------------------------
# a201037jf@i26:/var/tmp/m01_operatius$ bash 02-exemple-args.sh aa bb cc dd ee ff gg hh ii jj kk
