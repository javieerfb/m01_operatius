#!/bin/bash
# 22 Marzo 2023
# HISX1 M01

opciones=""
argumentos=""

for elem in $*
do
  case $elem in
    "-a"|"-b"|"-c"|"-d")
    opciones="$opciones $elem";;
    *)
    argumentos="$argumentos $elem";;
  esac
done
echo "Opciones: $opciones"
echo "Argumentos: $argumentos"

