#!/bin/bash
# 22 Marzo 2023
# HISX1 M01
# 18-argsparse-sh [ -a -b -c -d -f file -n num ] args...
opciones=""
argumentos=""
myfile=""
mynum=""

while [ -n "$1" ]		#-n returns True if the lenght of STRING is nonzero
do
  case $1 in
    -[abcd])
	  opciones="$opciones $1";;
    -f)
	    shift
	myfile="$2";;
    -n)
	    shift
	mynum="$2";;
     *)
	argumentos="$argumentos $1";;
  esac
  shift
done
echo "Opciones: $opciones"
echo "Argumentos: $argumentos"
echo "File: $myfile"
echo "Num: $mynum"

