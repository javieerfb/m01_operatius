#!/bin/bash
# @edt ASIX M01
# 16 Febrero 2023
# 
# Validar que la nota facilitada da como resultado uno de los siguientes:
# Suspendido, aprovado, notable, excelente
# -------------------------------------------------
# 1) Validar que existe el argumento
if [ $# -ne 1 ]
then
  echo "Error: numero args incorrecto"
  echo "Usage: $0 nota"
  exit 1
fi

# 2) Validar que el argumento cumple el formato correcto
if [ $1 -lt 0 -o $1 -gt 10 ]
then
  echo "Error: la nota no puede ser menor que 0 ni mayor que 10"
  exit 2 
fi

# 3) Xixa
nota=$1
if [ $nota -lt 5 ]
then
  echo "$nota es Suspenso"
elif [ $nota -eq 7 -o $nota -eq 8 ]
then
  echo "$nota es Notable"
elif [ $nota -eq 9 -o $nota -eq 10 ]
then
  echo "$nota es Excelente"
else
  echo "Has aprobado, enhorabuena"
  exit 0
fi
