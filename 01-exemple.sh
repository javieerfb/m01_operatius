#!/bin/bash
# @edt ASIX M01
# 16 Febrero 2023
# 
# Ejemplo de primer programa
# Normas:
#	shebang (#!) Indica quien debe interpretar el fichero
#	cabecera, descripción, fecha, autor (obligatorio para aprobar)
# -------------------------------------------------------------------------
# Cosicas aprendidas:
## Script: Un conjunto de instrucciones
## Cuando hacemos	bash 01-exemple.sh	el echo $SHLVL nos indica que estamos en 2, porque al decir "bash" nos ejecuta el script en otra sesión bash
# -------------------------------------------------------------------------

echo "Hola mundo"
nom="pere pou prat"
edat=25
echo $nom $edat
echo -e "nom: $nom\n edat:$edat\n"
uname -a
uptime
echo $SHELL
echo $SHLVL
echo $((4*12))
echo $((edat * 2))

#read data1 data2
#echo -e "$data1 \n $data2"
exit 0

echo $?

