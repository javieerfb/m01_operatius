#!/bin/bash
# @edt ASIX M01
# 6 Marzo 2023
# 
# Exemples bucles for
# Descripción: 
# -------------------------------------------------


# 8) Listar los logins de los usuarios, numerados
contador=1
llistat=$(cut -d: -f1 /etc/passwd | sort)
for args in $llistat
do
  echo "$contador: $args"	
  ((contador++))
done
exit 0

# 7)	FALTA

# 6) Numerar los argumentos
contador=1
for args in $*
do
  echo "$contador. $args"
  contador=$((contador+1))
done
exit 0

# 5) Iterar i mostrar la lista d'arguments
#La diferencia entre el $* y el $@ es que el $@ se expandi incluso con comillas

for args in "$@"
do
  echo $args
done
exit 0

# 4) Iterar i mostrar la lista d'arguments
for args in $*
do
  echo $args
done
exit 0
   

# 3) Iterar un cojunto de elementos
llistat=$(ls)

for nom in $llistat
do
 echo "$nom"
done
exit 0


# 1) Iterar un cojunto de elementos
for nom in "pere" "marta" "pau" "anna"
do
  echo "$nom"
done
exit 0


# 1) Iterar un cojunto de elementos
for nom in "pere" "marta" "pau" "anna"
do
  echo "$nom"
done
exit 0
