#!/bin/bash
# @edt ASIX M01
# 16 Febrero 2023
# 
# Validar que tiene exactamente 2 argumentos y mostrarlos
# -------------------------------------------------
# 1) Validar que existe el argumento
# -ne --> not equal --> no igual
if [ $# -ne 2 ]
then
  echo "Error; numero args incorrecto"
  echo "Usage: $0 nom edat"
  exit 1
fi

# 2) Donde el programa hace lo que tiene que hacer
nom=$1
edad=$2
echo "nom es $nom, y tiene $edad años"
exit 0
