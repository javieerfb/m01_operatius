#!/bin/bash
# @edt ASIX M01
# 2 Marzo 2023
# 
# Exemples case
# -------------------------------------------------

# 1) Validar que existe el argumento
if [ $# -ne 1 ]
then
  echo "Error: numero args incorrecto"
  echo "Usage: $0 nota"
  exit 1
fi


case $1 in
	1|3|5|7|10|12)
		echo "El mes $1 tiene 31 dias"
		;;
	4|6|9|11)
		echo "El mes $1 tiene 30 dias"
		;;
	2)
		echo "El mes $1 tiene 28 o 29 dias depende de sí es bisiesto"
	
esac
exit 0
