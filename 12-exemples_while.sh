#!/bin/bash
# @edt ASIX M01
# 9 Marzo 2023
#
# Exemples bucle while
# -------------------------------------------------

# 7) Numerar stdin y mostrar en mayuscula
contador=0
while read -r line
do
	((contador++))
	echo "$contador: $line" | tr [a-z] [A-Z]
done
exit 0

# 6) Mostrar stdin linea a linea hasta token FI
## En este caso valido en el bucle una condicioón que YA existe!

read -r line
while [ "$line" != "FI"  ]
do
	echo $line
	read -r line
done
exit 0

# 5) Numerar stdin linea a linea
contador=0
while read -r line
do
	((contador++))
	echo "$contador: $line"
done
exit 0


# 4) Procesar entrada estandard linea a linia
while read -r line		#Mientras lea contenido en la entrada estandard la muestra, hasta finalizar el bucle con CTRL+D
do
	echo $line
done
exit 0	

# 3) Iterar por la lista de argumentos
while [ -n "$1" ]
do
  echo "$1 $# $*"
  shift
done
exit 0

# 2) contador decreciente n(arg)...0
arg=$1
while [ $arg -ne -1 ]
do
	echo $arg
	((arg--))
done
exit 0

# 1) Mostrar por pantalla del 1 al 100
limit=100
contador=0
while [ $contador -lt $limit ]
do
	((contador++))
	echo $contador
done
exit 0
