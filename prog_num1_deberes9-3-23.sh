#!/bin/bash
# @edt ASIX M01
# 1 Marzo 2023
# 
# 2) prog nota[...]
# El programa rep un o més números, el programa calcula i mostra la suma de tots aquests valors
# -------------------------------------------------
# 1) Validar que existe el argumento
if [ $# -eq 0 ]
then
  echo "Error: numero args incorrecto"
  echo "Usage: $0 nota"
  exit 1
fi

# 2) Xixa

suma=0
for num in "$*"
do
  suma=$suma+$num
done
echo "La suma de los números introducidos es $suma"
