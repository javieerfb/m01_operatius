#!/bin/bash
# @edt ASIX M01
# 16 Febrero 2023
# 
# Hacer un ls de un directorio
# -------------------------------------------------
# 1) Validar que existe el argumento
if [ $# -ne 1 ]
then
  echo "Error: numero args incorrecto"
  echo "Usage: $0 nota"
  exit 1
fi

# 2) Validar que el argumento sea un directorio (Con file operators (help test) )
if [ ! -d $1 ]
then
  echo "Error: el argumento especificado NO es un directorio"
  exit 2 
fi

# 3) Xixa
directorio=$1
ls $directorio
exit 0

