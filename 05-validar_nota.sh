#!/bin/bash
# @edt ASIX M01
# 16 Febrero 2023
# 
# Validar que la nota facilitada da como resultado un Aprobado o un Suspenso
# -------------------------------------------------
# 1) Validar que existe el argumento
if [ $# -ne 1 ]
then
  echo "Error: numero args incorrecto"
  echo "Usage: $0 nota"
  exit 1
fi

# 2) Validar que el argumento cumple el formato correcto
if [ $1 -gt 10 ]
then
  echo "Error: la nota no puede ser mayor que 10"
  exit 2 
fi

# 3) Xixa
nota=$1
if [ $nota -lt 5 ]
then
  echo "No has aprobado"
else
  echo "Has aprobado, enhorabuena"
  exit 0
fi
