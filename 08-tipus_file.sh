#!/bin/bash
# @edt ASIX M01
# 16 Febrero 2023
# 
# Validar que es el argumento facilitado:
# regular_file, directory, link
# -------------------------------------------------
# 1) Validar que existe el argumento
if [ $# -ne 1 ]
then
  echo "Error: numero args incorrecto"
  echo "Usage: $0 file"
  exit 1
fi

# 2) Xixa
if [ -f $1 ]
then
  echo "Es un fichero"
  exit 0 
elif [ -d $1 ]
then
  echo "Es un directorio"
  exit 0
elif [ -h $1 ]
then
  echo "Es un enlace"
  exit 0
else
  echo "No es ni un fichero regular, ni directorio, ni enlace"
  exit 0
fi
