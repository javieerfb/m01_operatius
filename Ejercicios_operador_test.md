Exercicis de test
1. file (element) not exists
a201037jf@i26:~$ [ ! -e /etc/os-inventado ] ; echo $?
0
a201037jf@i26:~$ [ ! -e /etc/os-release ] ; echo $?
1


2. is not a regular file
a201037jf@i26:~$ [ ! -f /etc/os-release ] ; echo $?
1


3. element in the filesystem exists
a201037jf@i26:~$ [ -e /etc/os-release ] ; echo $?
0


4. element is readable
a201037jf@i26:~$ [ -r /etc/os-release ] ; echo $?
0


5. num is not equal to 10
a201037jf@i26:~$ num=10 ; [ $num -ne 10 ] ; echo $?
1

6. num is less or equal to 100
a201037jf@i26:~$ num=10 ; [ $num -le 100 ] ; echo $?
0
a201037jf@i26:~$ num=101 ; [ $num -le 100 ] ; echo $?
1


7. name is not pere, not marta not anna
a201037jf@i26:~$ name="pepe" ; [ $name != "pere" -a $name != "marta" -a $name != "anna" ] ; echo $?
0
a201037jf@i26:~$ name="anna" ; [ $name != "pere" -a $name != "marta" -a $name != "anna" ] ; echo $?
1
a201037jf@i26:~$ name="pere" ; [ $name != "pere" -a $name != "marta" -a $name != "anna" ] ; echo $?
1


8. num is 10 or 15 or 20.
a201037jf@i26:~$ num=15 ; [ $num -eq 10 -o $num -eq 15 -o $num -eq 20 ] ; echo $?
0
a201037jf@i26:~$ num=20 ; [ $num -eq 10 -o $num -eq 15 -o $num -eq 20 ] ; echo $?
0
a201037jf@i26:~$ num=19 ; [ $num -eq 10 -o $num -eq 15 -o $num -eq 20 ] ; echo $?
1


9. num ins in the ranges [0,18], [65,120]
a201037jf@i26:~$ num=19 ; [ $num -ge 0 -a $num -le 18 -o $num -ge 65 -a $num -le 120 ] ; echo $?
1
a201037jf@i26:~$ num=17 ; [ $num -ge 0 -a $num -le 18 -o $num -ge 65 -a $num -le 120 ] ; echo $?
0
a201037jf@i26:~$ num=64 ; [ $num -ge 0 -a $num -le 18 -o $num -ge 65 -a $num -le 120 ] ; echo $?
1
a201037jf@i26:~$ num=110 ; [ $num -ge 0 -a $num -le 18 -o $num -ge 65 -a $num -le 120 ] ; echo $?
0


10. Num is not in the range [25,50[
a201037jf@i26:~$ num=110 ; ! [ $num -ge 25 -a $num -le 50 ] ; echo $?
0
a201037jf@i26:~$ num=25 ; ! [ $num -ge 25 -a $num -le 50 ] ; echo $?
1
a201037jf@i26:~$ num=51 ; ! [ $num -ge 25 -a $num -le 50 ] ; echo $?
0


11. error if argos not: arg1 arg2 arg3
[ #? -ne 3 ]

12. error if argos not: arg1...
[ #? ! -eq 0 ]    #Que no haya 1 o más argumentos... es decir 0

13. error if argos not: arg1 arg2...
[ #? -lt 2 ]		#Que no haya 2 o más argumenos... es decir argumentos menores que 2

14. var name has no value, is empty
a201037jf@i26:~$ name=pepe ; [ -z "$num" ] ; echo $?
1


15. var num is not null
a201037jf@i26:/tmp/prova$ num=34 ; [ -n "$num" ] ; echo $?
0

