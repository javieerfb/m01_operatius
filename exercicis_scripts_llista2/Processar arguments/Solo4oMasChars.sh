#!/bin/bash
# Processar els arguments i mostrar per stdout només els de 4 o més caràcters

# Debe tener por lo menos 1 arg:
if [ $# -eq 0 ]; then
	echo "ERROR: Debe tener 1 o mas args"
	exit 1
fi

# Solo mostrar sí tiene más de 4 char
for arg in $*
do
	echo "$arg" | grep -E '^.{4,}$'
done

exit 0

