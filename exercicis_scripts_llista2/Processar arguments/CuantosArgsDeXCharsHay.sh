#!/bin/bash
# Processar els arguments i comptar quantes n'hi ha de 3 o més caràcters.

# Debe tener por lo menos 1 arg:
if [ $# -eq 0 ]; then
	echo "ERROR: Debe tener 1 o mas args"
	exit 1
fi

# Código
contador=0
for arg in $*
do
	
	if [ $(echo "$arg" | grep -E '^.{3,}$' | echo $?) -eq 0 ];then
	((contador++))
	fi

done
echo "$contador"

exit 0

