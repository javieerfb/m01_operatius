#!/bin/bash
# @edt ASIX M01
# 16 Febrero 2023
# 
# Ejemplo if: indicar si es mayor de edad
# $ prog edat
# ----------------------------------------------------
## TEORIA:
# 1) Validar que existe el argumento
# -ne --> not equal --> no igual
if [ $# -ne 1 ]
then
  echo "Errir; numero args incorrecto"
  echo "Usage: $0 edad"
  exit 1
fi

# 2) Donde el programa hace lo que tiene que hacer
edad=$1
if [ $edad -ge 18 ] ; then
  echo "edad $edad es mayor de edad"
fi
exit 0
