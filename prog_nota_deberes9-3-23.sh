#!/bin/bash
# @edt ASIX M01
# 1 Marzo 2023
# 
# 2) prog nota[...]
# El programa rep una o més notes d'un alumne i indica per cada nota rebuda si és un suspès, 
# aprovat, notable o excel·lent.  Cal validar que almenys es rep una nota.
# Per cada nota cal validar que és un valor vàlid del zero al deu i mostrar 
# la nota i la seva qualificació en lletres (suspès, aprovat, notable o excel·lent).
# -------------------------------------------------
# 1) Validar que existe el argumento
if [ $# -eq 0 ]
then
  echo "Error: numero args incorrecto"
  echo "Usage: $0 nota"
  exit 1
fi

# 2) Validar que el argumento cumple el formato correcto
if [ $1 -lt 0 -o $1 -gt 10 ]
then
  echo "Error: la nota no puede ser menor que 0 ni mayor que 10"
  exit 2 
fi

# 3) Xixa

for nota in $*
do
	if [ $nota -lt 5 ]
	then
	  echo "$nota es Suspenso"
	elif [ $nota -eq 7 -o $nota -eq 8 ]
	then
	  echo "$nota es Notable"
	elif [ $nota -eq 9 -o $nota -eq 10 ]
	then
	  echo "$nota es Excelente"
	else
	  echo "$nota es Aprobado"
	  exit 0
	fi
done
